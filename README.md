# CSV2CalDav

Converts csv files to ics files. It provides a gui to load a csv file, set some parameters and convert it to an ics file. CSV2CalDav is limited to setting the following fields:
- Calendar name
- TimeZone
- Start date and time
- End date and time
- Summary
- Location
- Description
 
<p align="center"><img src="https://gitlab.com/raginggoblin/csv2caldav/raw/master/screenshots/CSV2CalDav.png" /></p>

# How to run

1. CSV2CalDav requires java 8 or higher (http://www.oracle.com/technetwork/java/javase/downloads/index.html).
2. Download [CSV2CalDav.one-jar.jar](https://gitlab.com/raginggoblin/csv2caldav/-/jobs/74491489/artifacts/download) to a convenient location (Note that it is inside the zip under target).
3. On Linux run 'java -jar CSV2CalDav.one-jar.jar'. On Windows dubble click CSV2CalDav.one-jar.jar or run 'java -jar CSV2CalDav.one-jar.jar'.
