/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of CSV2CalDav.
 *
 *  SpeechLess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SpeechLess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SpeechLess.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.csv2caldav;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Logger;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;

import net.fortuna.ical4j.model.Date;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.model.property.Version;

public class GUI extends JFrame {

   private static final Logger LOG = Logger.getLogger(GUI.class);

   private JTextField fieldLocale;
   private List<JComboBox<String>> lineBoxes = new ArrayList<>();
   private List<MatchFieldPanel> lineFieldPanels = new ArrayList<>();
   private List<String> lines;
   private JTextField fieldSeparator;
   private JTextField fieldOpenFile;
   private JTextField fieldCalendarName;
   private JSpinner spinnerSkipLines;
   private SimpleDateFormat startFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
   private SimpleDateFormat endFormat = null;
   private TimeZone timeZone = null;

   public GUI() {
      super("CSV2CalDav");
      setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icons/File-CSV-icon.png")));
      setSize(600, 600);
      setDefaultCloseOperation(EXIT_ON_CLOSE);
      ScreenPositioner.centerOnScreen(this);
      initContentPanel();
   }

   private void initContentPanel() {
      JPanel contentPanel = new JPanel();
      getContentPane().add(contentPanel, BorderLayout.CENTER);
      contentPanel.setLayout(new FormLayout(new ColumnSpec[] {

            FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC,
            ColumnSpec.decode("default:grow"), FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default"),
            FormFactory.RELATED_GAP_COLSPEC, ColumnSpec.decode("default"), FormFactory.RELATED_GAP_COLSPEC,

      },

            new RowSpec[] { FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                  FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                  FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                  FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                  FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                  FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                  FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                  FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                  FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                  FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                  FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                  FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC,
                  FormFactory.DEFAULT_ROWSPEC, FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                  FormFactory.RELATED_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, }));

      StringSeparator openFileSeparator = new StringSeparator("1) Open CSV file");
      contentPanel.add(openFileSeparator, "1, 2, 8, 1");

      JButton btnOpenFile = new JButton(getIcon("/icons/folder_page.png"));
      btnOpenFile.addActionListener(ae -> openFile());
      contentPanel.add(btnOpenFile, "8, 4");

      fieldOpenFile = new JTextField("...");
      fieldOpenFile.setEnabled(false);
      contentPanel.add(fieldOpenFile, "2, 4, 4, 1");

      contentPanel.add(new JLabel("Delimiter: "), "2, 6, right, center");

      fieldSeparator = new JTextField(",");
      contentPanel.add(fieldSeparator, "4, 6, 2, 1");

      StringSeparator setPropertiesSeparator = new StringSeparator("2) Set calendar properties");
      contentPanel.add(setPropertiesSeparator, "1, 8, 8, 1");

      contentPanel.add(new JLabel("Calendar name: "), "2, 10, right, center");

      fieldCalendarName = new JTextField("CSV");
      contentPanel.add(fieldCalendarName, "4, 10, 2, 1");

      contentPanel.add(new JLabel("Locale: "), "2, 12, right, center");

      fieldLocale = new JTextField("Europe/Amsterdam");
      contentPanel.add(fieldLocale, "4, 12, 2, 1");

      contentPanel.add(new JLabel("Skip number of lines: "), "2, 14, right, center");

      spinnerSkipLines = new JSpinner(new SpinnerNumberModel(1, 0, Integer.MAX_VALUE, 1));
      contentPanel.add(spinnerSkipLines, "4, 14, left, default");

      StringSeparator setCSVFieldsSeparator = new StringSeparator("3) Match CSV columns");
      contentPanel.add(setCSVFieldsSeparator, "1, 16, 8, 1");

      for (int i = 0; i < 5; i++) {
         JComboBox<String> boxTextLine = new JComboBox<>();
         lineBoxes.add(boxTextLine);
         int yIndex = 18 + i * 2;
         contentPanel.add(boxTextLine, "2, " + yIndex);

         MatchFieldPanel matchPanel = new MatchFieldPanel();
         contentPanel.add(matchPanel, "4, " + yIndex);
         lineFieldPanels.add(matchPanel);
      }

      StringSeparator saveFileSeparator = new StringSeparator("4) Save to ICS file");
      contentPanel.add(saveFileSeparator, "1, 28, 8, 1");

      JButton saveButton = new JButton("   Save", getIcon("/icons/disk.png"));
      saveButton.addActionListener(e -> saveFile());
      contentPanel.add(saveButton, "2, 30, 7, 1");
   }

   private void openFile() {
      JFileChooser chooser = new JFileChooser();
      chooser.setFileFilter(new FileFilter() {

         @Override
         public String getDescription() {
            return "CSV files";
         }

         @Override
         public boolean accept(File f) {
            return f.isDirectory() || f.getName().endsWith(".csv");
         }
      });
      chooser.showOpenDialog(GUI.this);

      File file = chooser.getSelectedFile();
      fieldOpenFile.setText(file.getName());
      try {
         lines = Files.readAllLines(file.toPath());
         fillTextLineBoxes();
      } catch (IOException e) {
         LOG.error("Error reading file", e);
         JOptionPane.showMessageDialog(GUI.this, e.getMessage(), "Error reading file", JOptionPane.ERROR_MESSAGE);
      }
   }

   private void saveFile() {
      JFileChooser chooser = new JFileChooser();
      chooser.setFileFilter(new FileFilter() {

         @Override
         public String getDescription() {
            return "ICS files";
         }

         @Override
         public boolean accept(File f) {
            return f.isDirectory() || f.getName().endsWith(".ics");
         }
      });
      chooser.showOpenDialog(GUI.this);
      File file = chooser.getSelectedFile();

      try {
         TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance().createRegistry();
         if (!fieldLocale.getText().equals("")) {
            timeZone = registry.getTimeZone(fieldLocale.getText());
         }

         net.fortuna.ical4j.model.Calendar caldav = new net.fortuna.ical4j.model.Calendar();
         caldav.getProperties().add(new ProdId(String.format("-//%s//iCal4j 1.0//EN", fieldCalendarName.getText())));
         caldav.getProperties().add(Version.VERSION_2_0);
         caldav.getProperties().add(CalScale.GREGORIAN);

         Optional<MatchFieldPanel> startPanel = lineFieldPanels.stream()
               .filter(p -> p.matchComboBox.getSelectedItem() == MatchFields.Start).findFirst();
         if (startPanel.isPresent()) {
            startFormat = new SimpleDateFormat(startPanel.get().dateFormatField.getText());
         } else {
            JOptionPane.showMessageDialog(GUI.this, "You at least have to pick a start field",
                  "Error saving calendar file", JOptionPane.ERROR_MESSAGE);
         }

         Optional<MatchFieldPanel> endPanel = lineFieldPanels.stream()
               .filter(p -> p.matchComboBox.getSelectedItem() == MatchFields.End).findFirst();
         if (endPanel.isPresent()) {
            endFormat = new SimpleDateFormat(endPanel.get().dateFormatField.getText());
         }

         final int startIndex = getIndex(getMatchFieldIndex(MatchFields.Start));
         final int endIndex = getIndex(getMatchFieldIndex(MatchFields.End));
         final int titleIndex = getIndex(getMatchFieldIndex(MatchFields.Title));
         final int descriptionIndex = getIndex(getMatchFieldIndex(MatchFields.Description));
         final int locationIndex = getIndex(getMatchFieldIndex(MatchFields.Location));

         lines.stream().skip((Integer) spinnerSkipLines.getValue()).forEach(line -> {
            String[] elements = line.split(fieldSeparator.getText(), -1);
            try {
               Date start = new DateTime(startFormat.parse(elements[startIndex]));
               Date end = null;
               if (endIndex >= 0) {
                  end = new DateTime(endFormat.parse(elements[endIndex]));
               }
               String title = titleIndex >= 0 ? elements[titleIndex] : null;
               String location = locationIndex >= 0 ? elements[locationIndex] : null;
               String description = descriptionIndex >= 0 ? elements[descriptionIndex] : null;

               VEvent event = createEvent(timeZone, start, end, title, location, description);
               caldav.getComponents().add(event);
            } catch (Exception e) {
               JOptionPane.showMessageDialog(GUI.this, e.getMessage(), "Error saving calendar file",
                     JOptionPane.ERROR_MESSAGE);
            }
         });

         Files.write(Paths.get(file.toURI()), caldav.toString().getBytes("utf-8"), StandardOpenOption.CREATE,
               StandardOpenOption.TRUNCATE_EXISTING);

      } catch (Exception e) {
         JOptionPane.showMessageDialog(GUI.this, e.getMessage(), "Error saving calendar file",
               JOptionPane.ERROR_MESSAGE);
      }
   }

   private int getIndex(int matchFieldIndex) {
      if (matchFieldIndex >= 0) {
         JComboBox<String> lineBox = lineBoxes.get(matchFieldIndex);
         String[] lineHeads = lines.get(0).split(fieldSeparator.getText(), -1);
         for (int i = 0; i < lineHeads.length; i++) {
            if (lineBox.getSelectedItem() != null && lineHeads.length > i
                  && lineBox.getSelectedItem().equals(lineHeads[i])) {
               return i;
            }
         }
      }
      return -1;
   }

   private int getMatchFieldIndex(MatchFields field) {
      for (int i = 0; i < lineFieldPanels.size(); i++) {
         if (lineFieldPanels.get(i).matchComboBox.getSelectedItem() == field) {
            return i;
         }
      }
      return -1;
   }

   private void fillTextLineBoxes() {
      if (!lines.isEmpty()) {
         String[] fields = lines.get(0).split(fieldSeparator.getText(), -1);
         lineBoxes.stream().forEach(b -> {
            b.addItem("Select field");
            Arrays.stream(fields).forEach(f -> b.addItem(f));
         });
      }
   }

   private ImageIcon getIcon(String resource) {
      return new ImageIcon(getClass().getResource(resource));
   }

   private static VEvent createEvent(TimeZone timeZone, Date start, Date end, String title, String location,
         String description) {
      if (end == null) {
         end = start;
      }
      VEvent event = new VEvent(start, end, title);
      event.getProperties().add(new Uid(UUID.randomUUID().toString()));
      if (description != null) {
         event.getProperties().add(new Description(description));
      }
      if (location != null) {
         event.getProperties().add(new Location(location));
      }
      if (timeZone != null) {
         event.getProperties().add(timeZone.getVTimeZone().getTimeZoneId());
      }
      return event;
   }

   private enum MatchFields {
      Select, Start, End, Title, Description, Location
   };

   private class MatchFieldPanel extends JPanel {

      private JTextField dateFormatField = new JTextField("dd-MM-yyyy HH:mm:ss");
      private JComboBox<MatchFields> matchComboBox = new JComboBox<>(MatchFields.values());

      public MatchFieldPanel() {
         add(dateFormatField);
         dateFormatField.setEnabled(false);
         add(matchComboBox);
         matchComboBox
               .addActionListener(e -> dateFormatField.setEnabled(matchComboBox.getSelectedItem() == MatchFields.Start
                     || matchComboBox.getSelectedItem() == MatchFields.End));
      }
   }
}
