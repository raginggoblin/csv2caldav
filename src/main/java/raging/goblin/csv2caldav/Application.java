/*
 * Copyright 2015, RagingGoblin <http://raginggoblin.wordpress.com>
 *
 * This file is part of CSV2CalDav.
 *
 *  SpeechLess is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  SpeechLess is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with SpeechLess.  If not, see <http://www.gnu.org/licenses/>.
 */

package raging.goblin.csv2caldav;

import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.log4j.Logger;

public class Application {

	private static final Logger LOG = Logger.getLogger(Application.class);

	public static void main(String[] args) {
		loadLaf();
		GUI gui = new GUI();
		gui.setVisible(true);
	}

	private static void loadLaf() {
		try {
			boolean isGtk = tryLinuxLaf();
			if (!isGtk) {
				if (UIManager.getSystemLookAndFeelClassName().contains("Metal")) {
					setNimbusLaf();
				} else {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					LOG.debug("Setting system look and feel");
				}
			}
		} catch (Exception e) {
			LOG.error("Could not set system look and feel");
			LOG.debug(e.getMessage(), e);
		}
	}

	private static boolean tryLinuxLaf() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
	UnsupportedLookAndFeelException {
		for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			if ("GTK+".equals(info.getName())) {
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
				LOG.debug("Setting GTK+ look and feel");
				return true;
			}
		}
		return false;
	}

	private static void setNimbusLaf() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
	UnsupportedLookAndFeelException {
		for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
			if ("Nimbus".equals(info.getName())) {
				UIManager.setLookAndFeel(info.getClassName());
				LOG.debug("Setting Nimbus look and feel");
				break;
			}
		}
	}

}
